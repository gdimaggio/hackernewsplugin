//
//  HackerNews.swift
//  HackerNews
//
//  Created by Gianluca Di maggio on 2/10/17.
//  Copyright © 2017 dima. All rights reserved.
//

import UIKit

@objc(HackerNewsSwift) public class HackerNews: CDVPlugin {

    static var token: String?

    @objc(initSDK:)
    func initSDK(command: CDVInvokedUrlCommand) -> Void {

    }

    @objc(showHackerNews:)
    func showHackerNews(command: CDVInvokedUrlCommand) -> Void {
        guard let rootViewController = UIApplication.topViewController() else { return }

        let storyboard = UIStoryboard(name: "HackerNews", bundle: Bundle(for: HackerNews.self))
        if let controller = storyboard.instantiateInitialViewController() {
            rootViewController.present(controller, animated: true, completion: nil)
        }
    }

}
