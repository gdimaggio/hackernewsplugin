//
//  HNUtils.swift
//  HackerNews
//
//  Created by Gianluca Di maggio on 2/9/17.
//  Copyright © 2017 dima. All rights reserved.
//

import Foundation

extension UIApplication {

    static func topViewController() -> UIViewController? {
        var topController: UIViewController? = UIApplication.shared.windows.first?.rootViewController
        var isPresenting = false
        repeat {
            if let controller = topController {
                if let presented = controller.presentedViewController {
                    isPresenting = true
                    topController = presented
                } else {
                    isPresenting = false
                }
            }
        } while isPresenting
        return topController
    }
    
}
