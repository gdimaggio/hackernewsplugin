//
//  HNNewsTableViewCell.swift
//  HackerNews
//
//  Created by Gianluca Di maggio on 2/9/17.
//  Copyright © 2017 dima. All rights reserved.
//

import UIKit

class HNNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var overlayView: UIView!

    static var cellIdentifier: String {
        return "NewsTableViewCellId"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        self.overlayView.isHidden = false
        self.overlayView.alpha = 1
    }

}
