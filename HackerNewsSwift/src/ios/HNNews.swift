//
//  HNNews.swift
//  HackerNews
//
//  Created by Gianluca Di maggio on 2/9/17.
//  Copyright © 2017 dima. All rights reserved.
//

import UIKit

struct HNNews {

    let uuid: Int
    let title: String
    let author: String
    let timestamp: TimeInterval
    let score: Int
    let url: URL

    init(uuid: Int, title: String, author: String, score: Int, timestamp: TimeInterval, url: URL) {
        self.uuid = uuid
        self.title = title
        self.author = author
        self.timestamp = timestamp
        self.score = score
        self.url = url
    }

}
