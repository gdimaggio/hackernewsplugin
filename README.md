# README #

## Installation ##

To add HackerNewsSwift plugin to your app, first add the iOS platform to your project:

> cordova platform add ios

Then add the HackerNews plugin using its relative path on the filesystem:

> cordova plugin add ..path/to/hackernewsplugin/HackerNewsSwift/

## Usage ##

To display the news feed when your app launches, add the following snippet of code to your *index.js* file - for example in the *onDeviceReady* function:

```js
onDeviceReady: function() {
    HackerNewsSwift.showHackerNews(
        function(msg) {
        },
        function(err) {
        }
    );
}
```
